#include <SoftwareSerial.h>
#include <stdio.h>
#include <string.h>

#define rxPin 2
#define txPin 3

SoftwareSerial GPSserial =  SoftwareSerial(rxPin, txPin);
char line[100];
int i = 0;
char head[6];

void clearLine()
{
  int j =0 ;
  for(j =0 ;j <100 ;j++)
  line[j] = '\0';
}

void clearHead()
{
    int j =0 ;
  for(j =0 ;j <5 ;j++)
  head[j] = '\0';
}

void setup() {
  Serial.begin(9600);
    
  GPSserial.begin(9600);
    
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  
     GPSserial.listen();

  if (GPSserial.isListening()) {
   Serial.println("Port One is listening!");
}else{
   Serial.println("Port One is not listening!");
}

}

void loop()
{  

  if (GPSserial.available()) {
    line[i] = GPSserial.read();
    if(line[i] == '\n')
    {
      i = 0;  
      if (strncmp (line,"$GPGLL",5) == 0)
      {      
        Serial.print(line);
      }
        clearLine();
    }
    else
     i++;
    
  }
  
  /*
  if (Serial.available()) {
    GPSserial.write(Serial.read());
  }*/
  
}

